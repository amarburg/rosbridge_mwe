A minimum working example for testing [rosbridge](http://wiki.ros.org/rosbridge_suite) and [rostful](https://rostful.readthedocs.io/en/latest/).   Builds (more or less) on the [beginner_tutorial](http://wiki.ros.org/ROS/Tutorials) for roscpp.

This MWE targets **ROS Noetic with Python 3**

# Getting started

The `rosbridge_suite` ROS package must be installed.  On Ubuntu, we can use the apt package:

`
sudo apt install ros-noetic-rosbridge-suite
`

*(insert rosdep magic here)*

If you plan to test the `rosbridge` functionality, you also need to install [roslibpy](https://roslibpy.readthedocs.io/en/latest/) in the environment where the clients (e.g., the scripts in [`rosbridge_mwe/scripts`](scripts/)) are going to be run.  Note that because rosbridge is rosbridge, the clients can be run on any host on the network; it does not need to have ROS installed.

A copy of the `main` branch of [Aaron's fork of Rostful](https://github.com/apl-ocean-engineering/rostful) is **required** (the "original" Rostful was written for earlier versions of ROS w/ Python 2).  This repository contains a `.repos` file which will pull the appropriate dependency:

```
cd src
vcs import < rosbridge_mwe/rosbridge_mwe.repos
```

Then build the Catkin workplace and launch

```
catkin build
source devel/setup.bash
roslaunch rosbridge_mwe rosbridge_mwe.launch
```

## Testing Rosbridge

The [query_talker.py](scripts/query_talker.py) uses [roslibpy](https://roslibpy.readthedocs.io/en/latest/) to call the rosbridge server.


## Testing rostful

The launchfile starts a rostful server on port 8086.  

* [http://localhost:8086/_rosdef](http://localhost:8086/_rosdef) will show a list of the server's topics and services.
* [http://localhost:8086/chatter](http://localhost:8086/chatter) will show the latest from the `/chatter` topic




