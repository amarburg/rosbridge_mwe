#include "ros/ros.h"
#include "std_msgs/String.h"
#include "rosbridge_mwe/AddTwoInts.h"

#include <sstream>

bool add(rosbridge_mwe::AddTwoInts::Request  &req,
         rosbridge_mwe::AddTwoInts::Response &res)
{
  res.sum = req.a + req.b;
  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("sending back response: [%ld]", (long int)res.sum);
  return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "rosbridge_mwe");

    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    ros::Rate loop_rate(1);

    ros::ServiceServer service = n.advertiseService("add_two_ints", add);

    int count = 0;
    while (ros::ok()) {
        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world " << count;
        msg.data = ss.str();

        ROS_INFO("%s", msg.data.c_str());

        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
        ++count;
    }

  return 0;
}